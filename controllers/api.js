var express = require("express");
var router = express.Router();
var storage = require("../services/StorageService");
const uuidv1 = require("uuid/v1");

router.route("/").get(function(req, res) {
  storage.GetAllData(data => {
    res.json(data);
  });
});

router.route("/addClientInstance").post(function(req, res) {
  var uuid = uuidv1()
    .replace("-", "")
    .split("-")[0]
    .toUpperCase();

  var client_details = {
    client_id: uuid,
    runtime_id: req.body.runtime_id,
    cpu_numberOfCore: req.body.cpu_numberOfCore,
    cpu_archName: req.body.cpu_archName,
    cpu_modelName: req.body.cpu_modelName,
    ram_size: req.body.ram_size,
    date_joined: req.body.date_joined,
    is_active: 1
  };

  storage.AddClientInfo(client_details, message => {
    if (message == null) {
      res.status(200);
      res.json({ message: "SUCCESS", clientId: uuid });
    } else {
      res.status(200);
      res.json({ message: "ERROR" });
    }
  });
});

router.route("/addMonitingLog").post(function(req, res) {
  var mlog_details = {
    operation_id: req.body.operation_id,
    client_id: req.body.client_id,
    start_timestamp: req.body.start_timestamp,
    stop_timestamp: null,
    is_active: 1
  };
  // console.log(mlog_details);
  
  storage.GetURLWatch(urls => {
    if(urls != null){
// console.log(urls)
  storage.AddMonitorOperationLog(mlog_details, message => {
    if (message == null) {
      res.status(200);
      res.json({ message: "SUCCESS" ,urls});
    } else {
      res.status(200);
      res.json({ message: "ERROR" });
    }
  }); }else{

    res.status(200);
    res.json({ message: "NOURL" });
  } });
});


router.route("/addNetworkStatus").post(function(req, res) {
  // console.log(req.body);
  var mlog_details = {
    status_id: req.body.status_id,
    operation_id: req.body.operation_id,
    timestamp: req.body.timestamp,
    net_interface: "null",
    net_ipAddrs: "null",
    net_rtt: req.body.net_rtt,
    net_downlink: req.body.net_downlink,
    net_effectiveType: req.body.net_effectiveType
  };

  storage.AddNetworkStatus(mlog_details, message => {
    if (message == null) {
      res.status(200);
      res.json({ message: "SUCCESS" });
    } else {
      res.status(200);
      res.json({ message: "ERROR" });
    }
  });
});
//===============================================
router.route("/updateClientStopStauts/:client_id").get(function(req, res) {
  storage.UpdateClientActiveStatus(req.params.client_id, 0, message => {
    if (message == null) {
      res.status(200);
      res.json({ message: "QoS Monitor Successfully Unistalled" });
    } else {
      res.status(200);
      res.json({ message: "ERROR" });
    }
  });
});
router.route("/updateMonitorStopStauts/:operation_id").get(function(req, res) {
  storage.UpdateMonitorActiveStatus(
    req.params.operation_id,
    req.query.stop_timestamp,
    0,
    message => {
      // console.log(req.params.operation_id + " " + req.query.stop_timestamp);
      if (message == null) {
        res.status(200);
        res.json({ Server: "SUCCESS" });
      } else {
        res.status(200);
        res.json({ message: "ERROR" });
      }
    }
  );
});
router.route("/addSiteMonitored/").post(function(req, res) {
  // console.log(req.body);
  var uuid = uuidv1()
  .replace("-", "")
  .split("-")[0]
  .toUpperCase();

  var sitelog_details = {
    siteid: uuid,
    operation_id: req.body.operation_id,
    name: req.body.name,
    mainUrl: req.body.mainUrl,
    partialUrl: req.body.partialUrl,
    timestamp: new Date().toISOString(),
    loadtime: req.body.loadtime,
    net_rtt: req.body.net_rtt,
    net_downlink: req.body.net_downlink,
    net_effectiveType: req.body.net_effectiveType,
  };
console.log(sitelog_details);
  storage.AddSiteLog(sitelog_details, message => {
    if (message == null) {
      res.status(200);
      res.json({ message: "SUCCESS" });
    } else {
      res.status(200);
      res.json({ message: "ERROR" });
    }
  });
});
router.route("/addURLwatch/").post(function(req, res) {
  var genId = parseInt(Math.random()*100000000);
  var url_details = {
    id:genId,
    mainUrl: req.body.mainurl,
    partialUrl: req.body.partialurl
  };

  storage.AddURLWatch(url_details, data => {
    if (data == true) {
      res.status(200).send({genId}); 
    } else {
      res.status(500).send(); 
    }
  });
});
router.route("/deleteURL/:id").get(function(req, res) {
  id = req.params.id;

  storage.RemoveURLWatch(id, (data) => {
    if (data == true) {
      res.status(200).send({id}); 
    } else {
      res.status(500).send(); 
    }
  });
});
module.exports = router;
