var express = require("express");
var router = express.Router();
var storage = require("../services/StorageService");
var ReportPdfGenerator = require('../services/ReportGenerator');

router.route("/").get(function(req, res) {
  storage.GetURLWatch(urls => {
  storage.GetAllCients(data => {
    res.render("home", { title: "Home", clients: data ,urls});
  });
});
});

router.route("/monitoringSessions/:client_id").get(function(req, res) {
  storage.GetClientExWithId(req.params.client_id, data => {
    console.log(req.params.client_id);
    storage.GetMonitoringSessionWithClientId(req.params.client_id, mdata => {
      res.render("monitoringSessions", {
        title: "Monitorng Sessions",
        client: data[0],
        sessions: mdata
      });
    });
  });
});

router.route("/networkStatus/:operation_id").get(function(req, res) {
  storage.GetNetworkStatusWithOperationId(req.params.operation_id, mdata => {
    res.render("partialNetworkStatus", {
      session_id: req.params.operation_i,
      status: mdata
    });
  });
});
router.route("/allnetworkStatus/:client_id").get(function(req, res) {
  storage.GetClientExWithId(req.params.client_id, data => {
    storage.GetNetworkStatusWithClientId(req.params.client_id, mdata => {
      res.render("allNetworkStatus", {
        title: "All Network Status",
        client: data[0],
        status: mdata
      });
    });
  });
});
router.route("/allsitesmonitored/:client_id").get(function(req, res) {
  storage.GetClientExWithId(req.params.client_id, data => {
    storage.GetSiteLogsWithClientId(req.params.client_id, mdata => {
      res.render("SitesMonitored", {
        title: "All Sites Monitored",
        client: data[0],
        status: mdata,
        client_id:req.params.client_id,
        singleOperation: false,
      });
    });
  });
});
router.route("/getclient/:client_id").get(function(req, res) {
  storage.GetClientExWithId(req.params.client_id, data => {
    
      res.send({client: data[0]});
  });
});
router.route("/sitesmonitored/:client_id").get(function(req, res) {
    storage.GetSiteLogsWithOperationId(req.query.operation_id, mdata => {
      res.render("SitesMonitored", {
        title: "Sites Monitored :"+req.query.operation_id,
        status: mdata,
        client_id:req.params.client_id,
        operation_id: req.query.operation_id,
        singleOperation: true,
      });
    });
});
router.route("/report/:client_id/:site_id").get(function(req, res) {
  storage.GetClientExWithId(req.params.client_id, clientData => {
    storage.GetSiteLogsWithSiteId(req.params.site_id, siteData => {
      res.setHeader('Content-disposition', `inline; filename=test`)
      res.setHeader('Content-type', 'application/pdf')
          var doc = ReportPdfGenerator.GenerateReportPdf(clientData[0],siteData[0]);
            doc.pipe(res);
            doc.end();
    });
  });
});
module.exports = router;
