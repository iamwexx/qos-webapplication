/**
 * Module dependencies.
 */
const express = require("express");
var cors = require('cors')
const path = require("path");
const flash = require("req-flash");
const session = require("express-session");
const bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var moment = require("moment");
require("dotenv").config(); //Load environment variables from .env file,

/**
 * Create Express server.
 */
const app = express();
app.use(cors())


//import the routers
var index = require("./controllers/index");
var api = require("./controllers/api");

/**
 * Express configuration.
 */

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs"); // view engine setup
app.locals.moment = moment; // this makes moment available as a variable in every EJS page

app.use(logger("dev"));
app.use(
  session({
    secret: "nkwlejnvo824hrboviuyer78",
    resave: true,
    saveUninitialized: true
  })
);

app.use(flash());

/**
 * register routers to root paths
 */
app.use("/", index);
app.use("/api", api);
/**
 * Error Handler.
 */
// if (process.env.NODE_ENV === "development") {
//   // only use in development
//   app.use(errorHandler());
// }

module.exports = app;
