var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database("./database/app.db");

// db.all("SELECT * FROM clientMachineStatus", function(err,rows) {
//  rows.forEach(function(rows) {
//     console.log(rows.name,rows.number);
//   });
// });
// db.close();

module.exports = {
  AddClientInfo: function(clientInstanceData, callback) {
    //let placeholders = clientInstanceData.map(data => "(?)").join(",");
    db.run(
      "INSERT INTO clientExtensionInstance(client_id,cpu_numberOfCore,cpu_archName,cpu_modelName,ram_size,date_joined,is_active) VALUES ('" +
        clientInstanceData.client_id +
        "','" +
        clientInstanceData.cpu_numberOfCore +
        "','" +
        clientInstanceData.cpu_archName +
        "','" +
        clientInstanceData.cpu_modelName +
        "','" +
        clientInstanceData.ram_size +
        "','" +
        clientInstanceData.date_joined +
        "','" +
        clientInstanceData.is_active +
        "');",
      function(err) {
        if (err) {
          console.error(err.message);
          callback(err.message);
        }
        // console.log("success");
        callback(null);
      }
    );
  },
  AddMonitorOperationLog: function(log, callback) {
    db.run(
      "INSERT INTO monitorOperationLogs(operation_id,client_id,start_timestamp,stop_timestamp,is_active) VALUES ('" +
        log.operation_id +
        "','" +
        log.client_id +
        "','" +
        log.start_timestamp +
        "','" +
        log.stop_timestamp +
        "','" +
        log.is_active +
        "');",
      function(err) {
        if (err) {
          console.error(err.message);
          callback(err.message);
        }
        // console.log("success");
        callback(null);
      }
    );
  },
  AddNetworkStatus: function(status, callback) {
    db.run(
      "INSERT INTO networkStatus(status_id,	operation_id,net_interface,	net_ipAddrs,	net_rtt,	net_downlink,	net_effectiveType,timestamp_	) VALUES ('" +
        status.status_id +
        "','" +
        status.operation_id +
        "','" +
        status.net_interface +
        "','" +
        status.net_ipAddrs +
        "','" +
        status.net_rtt +
        "','" +
        status.net_downlink +
        "','" +
        status.net_effectiveType +
        "','" +
        status.timestamp +
        "');",
      function(err, rows) {
        if (err) {
          console.error(err.message);
          callback(err.message);
        }
        // console.log("success");
        callback(null);
      }
    );
  },
  /// ==========================UPDATE============================
  // ==============================================================
  UpdateClientActiveStatus: function(client_id, isActive, callback) {
    db.run(
      "UPDATE clientExtensionInstance SET is_active = " +
        isActive +
        " WHERE client_id ='" +
        client_id +
        "';",
      function(err) {
        // console.error(client_id + "--");
        if (err) {
          console.error(err.message);
          callback(err.message);
        }
        // console.log("success");
        callback(null);
      }
    );
  },
  UpdateMonitorActiveStatus: function(operation_id,stop_timestamp,isActive,callback) {
    db.run(
      "UPDATE monitorOperationLogs SET is_active = " +
        isActive +
        ",stop_timestamp = '" +
        stop_timestamp +
        "' WHERE operation_id ='" +
        operation_id +
        "';",
      function(err) {
        if (err) {
          console.error(err.message);
          callback(err.message);
        }
        // console.log("success");
        callback(null);
      }
    );
  },
  //=======================================GET======================
  //===================================================================
  GetAllCients: function(callback) {
    db.all("SELECT * FROM clientExtensionInstance", (err, rows) => {
      if (err) {
        console.log(err);
        callback(null);
      }
      // console.log(rows);
      callback(rows);
    });
  },

  GetClientExWithId: function(client_id, callback) {
    db.all(
      "SELECT * FROM clientExtensionInstance WHERE client_id ='" +
        client_id +
        "'",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        // console.log(rows);
        callback(rows);
      }
    );
  },
  GetMonitoringSessionWithId: function(operation_id, callback) {
    db.all(
      "SELECT * FROM clientExtensionInstance WHERE operation_id ='" +
        operation_id +
        "'",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        console.log(rows);
        callback(rows);
      }
    );
  },
  GetMonitoringSessionWithClientId: function(client_id, callback) {
    db.all(
      "SELECT * FROM monitorOperationLogs WHERE client_id ='" + client_id + "'",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        // console.log(rows);
        callback(rows);
      }
    );
  },
  GetNetworkStatusWithOperationId: function(operation_id, callback) {
    db.all(
      "SELECT * FROM networkStatus WHERE operation_id ='" + operation_id + "';",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        // console.log(rows);
        callback(rows);
      }
    );
  },
  GetNetworkStatusWithClientId: function(client_id, callback) {
    db.all(
      "SELECT networkStatus.*, clientExtensionInstance.client_id FROM networkStatus INNER JOIN monitorOperationLogs ON monitorOperationLogs.operation_id = networkStatus.operation_id INNER JOIN clientExtensionInstance ON clientExtensionInstance.client_id = monitorOperationLogs.client_id  WHERE clientExtensionInstance.client_id ='" +
        client_id +
        "'",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        // console.log(rows);
        callback(rows);
      }
    );
  },
  GetSiteLogsWithSiteId: function(site_id, callback) {
    db.all(
      "SELECT * FROM sitesMonitored WHERE siteid ='" +
        site_id +
        "'",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        // console.log(rows);
        callback(rows);
      }
    );
  },
  GetSiteLogsWithClientId: function(client_id, callback) {
    db.all(
      "SELECT sitesMonitored.*, clientExtensionInstance.client_id FROM sitesMonitored INNER JOIN monitorOperationLogs ON monitorOperationLogs.operation_id = sitesMonitored.operation_id INNER JOIN clientExtensionInstance ON clientExtensionInstance.client_id = monitorOperationLogs.client_id  WHERE clientExtensionInstance.client_id ='" +
        client_id +
        "'",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        // console.log(rows);
        callback(rows);
      }
    );
  },
  GetSiteLogsWithOperationId: function(operation_id, callback) {
    db.all(
      "SELECT * FROM sitesMonitored WHERE operation_id ='" +
      operation_id +
        "'",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        // console.log(rows);
        callback(rows);
      }
    );
  },
  AddSiteLog: function(status, callback) {
    db.run(
      "INSERT INTO sitesMonitored(siteid,	operation_id,timestamp,	name,	mainUrl,	partialUrl,	loadtime, net_rtt,	net_downlink,	net_effectiveType	) VALUES ('" +
        status.siteid +
        "','" +
        status.operation_id +
        "','" +
        status.timestamp +
        "','" +
        status.name +
        "','" +
        status.mainUrl +
        "','" +
        status.partialUrl +
        "','" +
        status.loadtime +
        "','" +
        status.net_rtt +
        "','" +
        status.net_downlink +
        "','" +
        status.net_effectiveType +
        "');",
      function(err, rows) {
        if (err) {
          console.error(err.message);
          callback(err.message);
        }
        // console.log("success");
        callback(null);
      }
    );
  },
  AddURLWatch: function(status, callback) {
    db.run(
      "INSERT INTO urlWatch(id,mainUrl,partialUrl) VALUES ('" +
        status.id +
        "','" +
        status.mainUrl +
        "','" +
        status.partialUrl +
        "');",
      function(err) {

        if (err) {
          console.error(err);
          callback(null);
        }
        callback(true);
      }
    );
  },
  GetURLWatch: function(callback) {
    db.all(
      "SELECT * FROM urlWatch;",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        // console.log(rows);
        callback(rows);
      }
    );
  },
  RemoveURLWatch: function(id, callback) {
    db.run(
      "DELETE FROM urlWatch WHERE id = "+id +";",
      (err, rows) => {
        if (err) {
          console.log(err);
          callback(null);
        }
        // console.log(rows);
        callback(true);
      }
    );
  },
  
};
