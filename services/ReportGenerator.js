
const Printer = require('pdfmake')
const moment = require('moment');
const path = require('path');module.exports = {
    GenerateReportPdf: function(client, site){
        var printer = new Printer({
            Roboto: {
                normal: path.resolve('public', 'fonts', 'Roboto-Regular.ttf'),
                bold: path.resolve('public', 'fonts', 'Roboto-Bold.ttf'),
            } 
        });
        return printer.createPdfKitDocument({
            info: {
                title: `QOS REPORT`,
                author: 'Qos Monitoring Systems',
                subject: 'Report Page',
            },
            content: [{
                image: path.resolve('public', 'images', 'icon.png'),
                width: 150,
            },{
                text: `QoS REPORT: ${site.siteid}`,
                margin: [0,20,0,0],
                style: 'header'
            },
            {
                canvas:
                [
                {
                    type: 'rect',
                    x: 0, y: 11,
                    w: 500,
                    h: 5,
                    color: '#022445',
                    fillOpacity: 0.5
                }
                ]},
           
           {
            text: `Monitoring Report for the site XXX on ${moment(site.timestamp).format('LLL')}\n\n`,
           margin: [0, 8]
       },
            {
                 text: 'MONITORING EXTENTION : CLIENT DETAILS',
                style: 'subheader',
                margin: [0, 15]
            },{
                style: 'small',
                margin: [9, 0, 0, 8],
                table: {
                  widths: [120,90,1,120,110],
                  body: [
                    [{text: 'CLIENT ID :', bold: true},client.client_id,'|',"",""],
                            [{text: 'CPU CORE(s) :', bold: true}, client.cpu_numberOfCore,'|', {text: 'CPU ARCH :', bold: true},client.cpu_archName],
                            [{text: 'DATE JOINED :', bold: true}, moment(client.date_joined).format('DD/MM/YYYY, h:mm:ss a'),'|', {text: 'RAM SIZE :', bold: true},client.ram_size],
                            [{text: 'CPU MODEL :', bold: true}, {text: client.cpu_modelName, colSpan: 3 },'|',"",""],
                  ]
                },layout: 'headerLineOnly'
              },
              {
              canvas:
              [
              {
                  type: 'rect',
                  x: 5, y: 31,
                  w: 500,
                  h: 10,
                  color: '#7a7a7a',
                  fillOpacity: 0.5
              }
              ]},
              {
                text: 'CLOUD MONITORING DATA',
               style: 'subheader',
               margin: [0, 15]
           },{
            style: 'small',
            margin: [50, 10, 10, 10],
            table: {
              widths: [140,90],
              heights: 30,
              fontSize: 18,
              body: [
                [{text: 'SITE NAME :', bold: true},site.name],
                [{text: 'SITE URL :', bold: true},site.mainUrl],
                [{text: 'NETWORK EFFECTIVE TYPE :', bold: true},site.net_effectiveType],
                [{text: 'NETWORK RTT :', bold: true},site.net_rtt],
                [{text: 'NETWORK DOWNLINK :', bold: true},site.net_downlink],
                [{text: 'LOAD TIME :', bold: true},site.loadtime],
              ]
            },layout: 'headerLineOnly'
          },
          ],
            defaultStyle: {
                fontSize: 11,
                font: 'Roboto', // The font name was defined above.
                lineHeight: 1.2,
            },styles: {
                header: {
                    fontSize: 18,
                    bold: true
                },
                subheader: {
                    fontSize: 15,
                    bold: true
                },
                quote: {
                    italics: true
                },
                small: {
                    fontSize: 10
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                }
            }
        })
}

}



// {
//     title: `QOS REPORT`,
//     author: 'Qos Monitoring Systems',
//     subject: 'Report Page',
// },
// content: [{
//     image: path.resolve('public', '/images/icon.png'),
//     width: 150,
// },{
//     text: `QoS REPORT: <b>${site.siteid}</b>`,
//     margin: [0,20,0,0],
//     style: 'header'
// },
// `Monitoring Report for the site XXX on ${site.timestamp}\n\n`,
// {
//     text: 'MONITORING EXTENTION : CLIENT DETAILS',
//     style: 'subheader',
//     margin: [0, 15]
// },{
//     style: 'small',
//     margin: [9, 0, 0, 8],
//     table: {
//       widths: [120,90,1,120,110],
//       body: [
//         [{text: 'CLIENT ID :', bold: true},client.client_id,'|',{text: 'DATE JOINED :', bold: true}, client.date_joined]
//         [{text: 'CPU CORE(s) :', bold: true}, client.cpu_numberOfCore,'|', {text: 'CPU ARCH :', bold: true},client.cpu_archName],
//         [{text: 'CPU MODEL :', bold: true}, client.cpu_modelName,'|', {text: 'RAM SIZE :', bold: true},client.ram_size],
//       ]
//     },layout: 'headerLineOnly'
//   }